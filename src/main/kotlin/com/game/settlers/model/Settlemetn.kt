package com.game.settlers.model

/**
 * Created by Daniel Mironov on 22.03.2019
 */

open class Settlement(val tiles: List<Tile>) {


    init {
        if (tiles.size > 3) {
            throw IllegalArgumentException("Settlement can't have more than 3 tiles around it!")
        }
    }

    open fun calculateOutput(): Map<Resource, Double> {
        val result = emptyMap<Resource, Double>().toMutableMap()
        tiles.forEach { result[it.resource] = it.calculateResourceAmount() }
        return result.toMap()
    }
}