package com.game.settlers.model

/**
 * Created by Daniel Mironov on 22.03.2019
 */

class Player(val color: Color) {
    private val settlements: MutableList<Settlement> = emptyList<Settlement>().toMutableList()
    private var wheat: Double = 0.0
    private var clay: Double = 0.0
    private var stone: Double = 0.0
    private var wood: Double = 0.0
    private var lolCounter: Double = 0.0

    fun start(firstSettlement: Settlement, secondSettlement: Settlement) {
        settlements.add(firstSettlement)
        settlements.add(secondSettlement)
    }

    fun addSettlement(settlement: Settlement) {
        settlements.add(settlement)
    }

    fun getSettlementsList(): List<Settlement> {
        return settlements.toList()
    }

    fun upgradeSettlement(settlement: Settlement) {
        if (!settlements.contains(settlement)) {
            throw IllegalArgumentException("Cannot upgrade settlement - player does not have it")
        }
        settlements.remove(settlement)
        settlements.add(Town(settlement))
    }

    fun calculateIncome(): Map<Resource, Int> {
        settlements.forEach { settlement ->
            settlement.calculateOutput().forEach {
                addResource(it.key, it.value)
            }
        }
        return getRealIncome()
    }

    private fun getRealIncome(): Map<Resource, Int> {
        val realWheat = wheat.toInt()
        wheat -= realWheat.toDouble()
        val realClay = clay.toInt()
        clay -= realClay.toDouble()
        val realStone = stone.toInt()
        stone -= realStone.toDouble()
        val realWood = wood.toInt()
        wood -= realWood.toDouble()
        return mapOf(
            Pair(Resource.WHEAT, realWheat),
            Pair(Resource.CLAY, realClay),
            Pair(Resource.STONE, realStone),
            Pair(Resource.WOOD, realWood)
        )
    }

    private fun addResource(resource: Resource, amount: Double) {
        when (resource) {
            Resource.WHEAT -> wheat += amount
            Resource.CLAY -> clay += amount
            Resource.STONE -> stone += amount
            Resource.WOOD -> wood += amount
            Resource.NONE -> lolCounter += 1.0
        }
    }
}