package com.game.settlers.model

import com.game.settlers.mappers.mapPlayerToView
import com.game.settlers.ui.GameState
import com.game.settlers.view.PlayerView

/**
 * Created by Daniel Mironov on 22.03.2019
 */

class GameStateImpl : GameState {
    private val players: List<Player> =
        listOf(Player(Color.RED), Player(Color.BLACK), Player(Color.YELLOW), Player(Color.WHITE))

    override fun getPlayersViews(): List<PlayerView> {
        return players.map { mapPlayerToView(it) }
    }

    override fun start(settlementsPerPlayer: Map<Color, Set<Settlement>>) {
        for (player in players) {
            val currentSettlements = settlementsPerPlayer[player.color]
                ?: throw IllegalArgumentException("Each player must have start settlements!")
            if (currentSettlements.size != 2) {
                throw IllegalArgumentException("Each player must have exactly 2 start settlements!")
            }
            player.start(currentSettlements.first(), currentSettlements.last())
        }
    }

    override fun nextTurn(): Map<Color, Map<Resource, Int>> {
        val result: MutableMap<Color, Map<Resource, Int>> = HashMap()
        players.forEach {
            result[it.color] = it.calculateIncome()
        }
        return result.toMap()
    }

    override fun addSettlement(settlement: Settlement, color: Color) {
        getPlayerByColor(color).addSettlement(settlement)
    }

    override fun upgradeSettlement(settlement: Settlement, color: Color) {
        getPlayerByColor(color).upgradeSettlement(settlement)
    }

    private fun getPlayerByColor(color: Color): Player {
        return players.firstOrNull { it.color == color } ?: throw IllegalArgumentException("No player with that color!")
    }

}