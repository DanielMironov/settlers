package com.game.settlers.model

/**
 * Created by Daniel Mironov on 23.03.2019
 */

enum class Color {
    RED, BLACK, WHITE, YELLOW
}