package com.game.settlers.model

import java.util.*

/**
 * Created by Daniel Mironov on 22.03.2019
 */

class Tile(val resource: Resource, private val digit: Int) {

    val id: UUID = UUID.randomUUID()

    init {
        if ((digit < 2) || (digit > 12)) {
            throw IllegalArgumentException("Digit on a tile must be between 2 an 12!")
        }
    }

    fun calculateResourceAmount(): Double {
        return calculateChanceFromDigit() * incomeCoefficient
    }

    private fun calculateChanceFromDigit(): Double {
        var i = 1
        var j = 1
        var counter = 0
        while (i < 7) {
            while (j < 7) {
                if (i + j == digit) {
                    counter++
                }
                j++
            }
            j = 1
            i++
        }
        return counter / 36.0
    }
}