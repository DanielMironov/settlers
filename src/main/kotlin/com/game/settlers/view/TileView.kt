package com.game.settlers.view

import com.game.settlers.model.Resource

/**
 * Created by Daniel Mironov on 05.04.2019
 */

data class TileView(val income: Double, val resource: Resource)