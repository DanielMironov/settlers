package com.game.settlers.view

import com.game.settlers.model.Color

/**
 * Created by Daniel Mironov on 05.04.2019
 */

data class PlayerView(val settlements: List<SettlementView>, val color: Color)