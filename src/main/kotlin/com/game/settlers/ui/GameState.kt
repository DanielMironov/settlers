package com.game.settlers.ui

import com.game.settlers.model.Color
import com.game.settlers.model.Player
import com.game.settlers.model.Resource
import com.game.settlers.model.Settlement
import com.game.settlers.view.PlayerView

/**
 * Created by Daniel Mironov on 05.04.2019
 */

interface GameState {

    fun getPlayersViews(): List<PlayerView>

    fun start(settlementsPerPlayer: Map<Color, Set<Settlement>>)

    fun nextTurn(): Map<Color, Map<Resource, Int>>

    fun addSettlement(settlement: Settlement, color: Color)

    fun upgradeSettlement(settlement: Settlement, color: Color)
}