package com.game.settlers.ui

import com.game.settlers.model.Color
import com.game.settlers.model.Resource

/**
 * Created by Daniel Mironov on 05.04.2019
 */

class ConsoleUI(override val gameState: GameState) : AbstractGameUI() {

    override fun showState() {
        for (player in gameState.getPlayersViews()) {
            val townCount = player.settlements.filter { it.isTown }.count()
            val settlementsCount = player.settlements.count() - townCount
            println(
                player.color.name + " player has " + settlementsCount + " settlements and "
                        + townCount + " towns"
            )
        }
    }

    override fun showIncome(incomePerPlayer: Map<Color, Map<Resource, Int>>) {
        println("NEXT TURN")
        for (player in incomePerPlayer) {
            for (resource in player.value) {
                if (resource.value>0) {
                    println(player.key.name + " gets " + resource.value + " of " + resource.key.name)
                }
            }
        }
    }
}