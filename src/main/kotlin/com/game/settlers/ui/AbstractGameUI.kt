package com.game.settlers.ui

import com.game.settlers.model.Color
import com.game.settlers.model.Resource
import com.game.settlers.model.Settlement

/**
 * Created by Daniel Mironov on 05.04.2019
 */

abstract class AbstractGameUI {

    abstract val gameState: GameState

    abstract fun showState()

    open fun nextTurn() {
        val incomePerPlayer = gameState.nextTurn()
        showIncome(incomePerPlayer)
    }

    open fun start(settlementsPerPlayer: Map<Color, Set<Settlement>>) {
        gameState.start(settlementsPerPlayer)
    }

    abstract fun showIncome(incomePerPlayer: Map<Color, Map<Resource, Int>>)
}