package com.game.settlers.mappers

import com.game.settlers.model.Player
import com.game.settlers.model.Settlement
import com.game.settlers.model.Tile
import com.game.settlers.model.Town
import com.game.settlers.view.PlayerView
import com.game.settlers.view.SettlementView
import com.game.settlers.view.TileView

/**
 * Created by Daniel Mironov on 05.04.2019
 */

fun mapPlayerToView(player: Player): PlayerView {
    val settlements = player.getSettlementsList().map { mapSettlementToView(it) }
    return PlayerView(settlements, player.color)
}

fun mapSettlementToView(settlement: Settlement): SettlementView {
    val tiles = settlement.tiles.map { mapTileToView(it) }
    val isTown = (settlement is Town)
    return SettlementView(tiles, isTown)
}

fun mapTileToView(tile: Tile): TileView {
    return TileView(tile.calculateResourceAmount(), tile.resource)
}