package com.game.settlers

import com.game.settlers.model.*
import com.game.settlers.ui.AbstractGameUI
import com.game.settlers.ui.ConsoleUI
import com.game.settlers.ui.GameState

/**
 * Created by Daniel Mironov on 05.04.2019
 */

fun main() {
    val tile1 = Tile(Resource.CLAY, 3)
    val tile2 = Tile(Resource.CLAY, 8)
    val tile3 = Tile(Resource.CLAY, 2)
    val tile4 = Tile(Resource.WOOD, 12)
    val tile5 = Tile(Resource.WOOD, 10)
    val tile6 = Tile(Resource.WOOD, 6)
    val tile7 = Tile(Resource.STONE, 8)
    val tile8 = Tile(Resource.STONE, 9)
    val tile9 = Tile(Resource.STONE, 3)
    val tile10 = Tile(Resource.WHEAT, 6)
    val tile11 = Tile(Resource.WHEAT, 5)
    val tile12 = Tile(Resource.WHEAT, 4)
    val settlement1 = Settlement(listOf(tile1, tile2, tile3))
    val settlement2 = Settlement(listOf(tile4, tile5, tile6))
    val settlement3 = Settlement(listOf(tile7, tile8, tile9))
    val settlement4 = Settlement(listOf(tile10, tile11, tile12))
    val settlement5 = Settlement(listOf(tile1, tile3, tile6))
    val settlement6 = Settlement(listOf(tile2, tile4, tile8))
    val settlement7 = Settlement(listOf(tile3, tile6, tile12))
    val settlement8 = Settlement(listOf(tile4, tile7, tile11))
    val startMap =
        mapOf(
            Pair(Color.RED, setOf(settlement1, settlement2)),
            Pair(Color.WHITE, setOf(settlement3, settlement4)),
            Pair(Color.BLACK, setOf(settlement5, settlement6)),
            Pair(Color.YELLOW, setOf(settlement7, settlement8))
        )
    val gameState: GameState = GameStateImpl()
    val ui: AbstractGameUI = ConsoleUI(gameState)
    ui.start(startMap)
    ui.showState()
    ui.nextTurn()
    ui.nextTurn()
    ui.nextTurn()
}
